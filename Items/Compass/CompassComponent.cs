﻿using Microsoft.Xna.Framework;
using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.Compass.Items.Compass {
    public class CompassComponent {
        public readonly string Texture;
        public readonly Vector2 Size;
        public readonly Vector3F TextureOffset;

        public CompassComponent(Blob config) {
            Texture = config.GetString("texture");
            Size = config.FetchBlob("size").GetVector2F().ToVector2();
            TextureOffset = config.Contains("textureOffset")
                ? config.FetchBlob("textureOffset").GetVector3F()
                : Vector3F.Zero;
        }
    }
}