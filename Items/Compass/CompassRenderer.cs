﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NimbusFox.KitsuneCore.V2.Classes;
using NimbusFox.KitsuneCore.V2.UI.Elements;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Rendering;

namespace NimbusFox.Compass.Items.Compass {
    public class CompassRenderer : ItemRenderer {
        private const float Offset = 0.5f;

        public override void Render(Item item, DeviceContext graphics, ref Matrix4F itemMatrix, Vector3D playerPosition,
            Vector3D renderOrigin, ref Matrix4F matrix, Timestep renderTimestep, string animation, float animationCycle,
            RenderMode renderMode) {
            if (item is CompassItem compass) {
                var inhand = compass.Configuration.InHandOffset;
                var oM = matrix
                    .RotateUnitY180()
                    .Translate(compass.TextureOffset)
                    .Multiply(ref itemMatrix);
                compass.CompassDial.Render(graphics, compass.Heading.X, ref oM);
            }
        }
    }
}