﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NimbusFox.Compass.Items.Compass.Builder;
using NimbusFox.KitsuneCore.Dependencies.Newtonsoft.Json;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.KitsuneCore.V2.Classes;
using NimbusFox.KitsuneCore.V2.UI.Elements;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Core;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.Compass.Items.Compass {
    public class CompassItem : Item {
        private readonly CompassItemBuilder _builder;
        public TextureRectangleDrawable CompassDial;
        
        public Heading Heading { get; private set; }
        public Vector3F TextureOffset { get; private set; }
        private bool _needUpdate;

        public CompassItem(CompassItemBuilder builder) : base(builder.Kind()) {
            _builder = builder;
            _needUpdate = true;
        }

        public override void Update(Entity entity, Timestep step, EntityUniverseFacade entityUniverseFacade) {
            if (entity.PlayerEntityLogic != null) {
                if (entity.PlayerEntityLogic.Heading() != Heading) {
                    Heading = entity.PlayerEntityLogic.Heading();
                    _needUpdate = true;
                    entity.PlayerEntityLogic.Inventory().ItemStoreNeedsStorage();
                }
            }
        }

        public override void Control(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }

        protected override void AssignFrom(Item item) {
            if (item.GetItemCode() == NullItem.GetItemCode()) {
                return;
            }
            Configuration = item.Configuration;
        }

        public override bool PlacementTilePreview(AvatarController avatar, Entity entity, Universe universe, Vector3IMap<Tile> previews) {
            return false;
        }

        public override bool HasAssociatedToolComponent(Components components) {
            return false;
        }

        public override void Restore(ItemConfiguration configuration, Blob blob) {
            base.Restore(configuration, blob);

            var component = configuration.Components.Select<CompassComponent>().FirstOrDefault();

            if (component == default(CompassComponent)) {
                throw new Exception($"{configuration.Code} is missing the compass component");
            }

            TextureOffset = component.TextureOffset;

            if (Helpers.IsClient()) {
                if (CompassDial == null) {
                    CompassDial = new TextureRectangleDrawable(component.Size, new UIPicture(context =>
                        Texture2D.FromStream(context.Graphics.GraphicsDevice,
                            GameContext.ContentLoader.ReadStream(component.Texture))));
                }
            }

            if (blob.Contains("direction")) {
                if (blob.KeyValueIteratable["direction"].Kind == BlobEntryKind.String) {
                    Heading = JsonConvert.DeserializeObject<Heading>(blob.GetString("direction"));
                }
            }
        }

        public override bool Same(Item item) {
            if (item.GetItemCode() == NullItem.GetItemCode()) {
                return false;
            }
            return Configuration.Code == item.Configuration.Code;
        }

        public override void Store(Blob blob) {
            base.Store(blob);
            if (_needUpdate) {
                blob.SetString("direction", JsonConvert.SerializeObject(Heading));
                _needUpdate = false;
            }
        }

        public override ItemRenderer FetchRenderer() {
            return _builder.Renderer;
        }
    }
}