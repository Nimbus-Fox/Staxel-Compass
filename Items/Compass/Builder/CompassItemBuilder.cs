﻿using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.Compass.Items.Compass.Builder {
    public class CompassItemBuilder : IItemBuilderHelper {
        public ItemRenderer Renderer { get; private set; }
        
        public static string KindCode() {
            return "nimbusfox.compass.item.compass";
        }
        
        public void Dispose() {
            
        }

        public void Load() {
            Renderer = new CompassRenderer();
        }

        public Item Build(Blob blob, ItemConfiguration configuration, Item spare) {
            if (!(spare is CompassItem)) {
                var compass = (Item)new CompassItem(this);
                compass.Restore(configuration, blob);
                return compass;
            }
            
            spare.Restore(configuration, blob);
            return spare;
        }

        public string Kind() {
            return KindCode();
        }
    }
}