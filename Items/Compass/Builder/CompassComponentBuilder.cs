﻿using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.Compass.Items.Compass.Builder {
    public class CompassComponentBuilder : IComponentBuilder {
        public string Kind() {
            return "compass";
        }

        public object Instance(Blob config) {
            return new CompassComponent(config);
        }
    }
}